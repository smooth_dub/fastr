'''Test class definition'''
from fastr.utils import rewrite_path, extract_path_params, extract_query_params
from fastr.utils.oas3_utils import (generate_json_schemas,
                                    generate_optional_format, generate_writable_body, generate_writable_params)
from fastr.config import TEST_TEMPLATE, SCHEMA_CHECK_TEMPLATE
from fastr.config import Config
from fastr.config.security_type import SecurityType


class Test():
    """An optimal test case generated from a OAS3 contract.
    """

    def __init__(self, path: str, method: str, definition: dict) -> None:
        self.path = path
        self.definition = definition
        self.method = method.lower()
        self.query_params = extract_query_params(definition)
        self.path_params = extract_path_params(definition)
        self.body = []
        self.responses_schemas = generate_json_schemas(definition)

    def __str__(self) -> str:
        body = generate_writable_body(self.definition)
        params = generate_writable_params(self.query_params)
        return TEST_TEMPLATE.format(
            status_code=200,
            schemas=rewrite_path(self.path) + '_200',
            method=self.method,
            writable_path=rewrite_path(self.path),
            path=self.path,
            optional_format=generate_optional_format(self.path_params),
            header=self.__get_header(),
            params=params,
            body=body,
            params_injection=', params=parameters' if self.query_params else '',
            body_injection=', json=body' if body != '' else '',
            assert_response=self.__get_schema_check(200)
        )

    def __get_schema_check(self, return_code: int) -> str:
        if return_code in self.responses_schemas:
            return SCHEMA_CHECK_TEMPLATE.format(schema=rewrite_path(self.path) + '_200')
        return ''

    def __get_header(self) -> str:
        if Config().get_security_type() == SecurityType.JWT:
            return '{\'Authorization\':\'bearer \' + JWT}'
        if Config().get_security_type() == SecurityType.NONE:
            return '{}'
