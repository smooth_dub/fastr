"""TestCollection class definition"""

import os
import typing
from fastr.models.test import Test
from fastr.utils import rewrite_path, ask_overwrite_confirmation
from fastr.config import (Config, HEADER_TEMPLATE,
                          CONFIG_HEADER_TEMPLATE, POWERSHELL_EXEC_SCRIPT, BASH_EXEC_SCRIPT)


class TestCollection():
    """A collection of tests. It creates a standalone file when
    written.
    """

    def __init__(self, path: str, contract: dict) -> None:
        if not isinstance(path, str) or not isinstance(contract, dict):
            raise TypeError("Type error on TestCollection init.")
        self.path = path
        self.name = 'test_' + rewrite_path(path)
        self.tests = []
        for key, value in contract.items():
            self.tests.append(Test(path, key, value))

    def append(self, item: Test) -> None:
        """Add a Test in the collection.

        Args:
            item (Test): The test to add.

        Raises:
            TypeError: Invalid type passed as parameter.
        """
        if not isinstance(item, Test):
            raise TypeError(
                "TestCollection.append(item) accepts only Test objects.")
        self.tests.append(item)

    def write(self, directory: str) -> None:
        """Write the config file to the given directory.

        Args:
            directory (str): The output directory.

        Raises:
            OSError: Occurs when the directory is not correct.
        """
        if not os.path.isdir(directory):
            raise OSError(f"{directory} is not a directory.".format(directory))
        file = os.path.join(directory, self.name + '.py')
        overwrite_file = True
        if not Config().get_force_mode() and os.path.isfile(file):
            overwrite_file = ask_overwrite_confirmation(file)
        if overwrite_file:
            with open(file, 'w+') as document:
                document.write(str(self))

    def __str__(self) -> str:
        return HEADER_TEMPLATE.format(self.__aggregate_tests())

    def __aggregate_tests(self) -> str:
        """Aggregate all writable test to be printed in global write function.

        Returns:
            str: The aggregate of all Tests in str format.
        """
        result = ''
        for test in self.tests:
            result = result + str(test)
        return result

    @staticmethod
    def generate_test_collections_from_contract(contract: dict) -> typing.List:
        """Generate a list of TestCollection from the given OAS3 given contract.

        Args:
            contract (dict): The OAS3 contract.

        Returns:
            typing.List[TestCollection]: A list of TestCollection based on the given contract
        """
        collections = []
        for key, value in contract['paths'].items():
            collections.append(TestCollection(key, value))
        return collections

    @staticmethod
    def write_config_file(directory: str) -> None:
        """Write the configuration file with global variables.

        Args:
            directory (str): directory where the file should be created.
        """
        file = os.path.join(directory, 'config' + "." + 'py')
        content = CONFIG_HEADER_TEMPLATE
        with open(file, 'w+') as document:
            document.write(content)

    @staticmethod
    def write_scripts(directory: str) -> None:
        """Write both starts scripts.

        Args:
            directory (str): directory where the files should be created.
        """
        file = os.path.join(directory, 'run_test' + "." + 'ps1')
        content = POWERSHELL_EXEC_SCRIPT
        with open(file, 'w+') as document:
            document.write(content)

        file = os.path.join(directory, 'run_test' + "." + 'sh')
        content = BASH_EXEC_SCRIPT
        with open(file, 'w+') as document:
            document.write(content)

    @staticmethod
    def write_init_file(directory: str) -> None:
        """Write the configuration file with global variables.

        Args:
            directory (str): directory where the file should be created.
        """
        file = os.path.join(directory, '__init__' + "." + 'py')
        with open(file, 'w+') as document:
            document.write('')

    @staticmethod
    def write_schemas_file_from_test_collection(directory: str, collections: typing.List) -> None:
        """Creates the schemas files based on a list of test collections.

        Args:
            directory (str): output directory.
            collections (typing.List[TestCollection]): The list of collection from
            witch the file should be created.
        """
        file = os.path.join(directory, 'schemas' + '.py')
        content = ''
        for collection in collections:
            for test in collection.tests:
                for key, value in test.responses_schemas.items():
                    content += f"\n{rewrite_path(test.path) + '_' + str(key)} = {value}"
        with open(file, 'w+') as document:
            document.write(content)
