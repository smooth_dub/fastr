'''Tests for config module'''
from fastr.config import Config


def test_config_singleton():
    config = Config()
    config.force_mode = True
    assert Config().force_mode, True
