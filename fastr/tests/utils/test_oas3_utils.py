'''Tests for OAS3 utils'''

import yaml
import json
import os
import pytest
from jsonschema import validate
from fastr.utils.oas3_utils import generate_optional_format, generate_writable_params, generate_writable_body, generate_json_schemas

query_params = ['first', 'second', 'third']
global unicode
unicode = str


def test_generate_optional_format_ok():
    result = generate_optional_format(query_params)
    expected = ".format('first', 'second', 'third')"
    assert result == expected


def test_generate_writable_params_ok():
    result = generate_writable_params(query_params)
    expected = '''
    parameters = {
        'first' : '',
        'second' : '',
        'third' : ''
    }'''
    assert result == expected


def test_generate_writable_body_simple_ok():
    definition = None
    with open(os.path.join(os.path.dirname(
            __file__), '..', 'resources', 'utils', 'generate_writable_body_simple.yaml')) as document:
        definition = yaml.load(document, yaml.FullLoader)
    result = generate_writable_body(definition)
    expected = "\tbody = {\n\t\tid = '',\n\t\tname = '',\n\t\tphotoUrls = '',\n\t\ttags = '',\n\t\tstatus = ''\n\t}"
    assert result == expected


def test_generate_json_schemas_ok():
    definition = None
    with open(os.path.join(os.path.dirname(
            __file__), '..', 'resources', 'utils', 'generate_writable_body_simple.yaml')) as document:
        definition = yaml.load(document, yaml.FullLoader)
    result = generate_json_schemas(definition)[200]
    data = json.loads(
        '[[{"name" : "johan","photoUrls" : [],"tags" : [],"status": "available"},{"name" : "johan","photoUrls" : [],"tags" : [],"status": "available"}]]')
    try:
        validate(instance=data, schema=result)
    except Exception as e:
        pytest.fail(e)


def test_generate_json_schemas_nok():
    definition = None
    with open(os.path.join(os.path.dirname(
            __file__), '..', 'resources', 'utils', 'generate_writable_body_simple.yaml')) as document:
        definition = yaml.load(document, yaml.FullLoader)
    result = generate_json_schemas(definition)[200]
    data = json.loads(
        '[[{"ne" : "johan","photoUrls" : [],"tags" : [],"status": "available"},{"name" : "johan","photoUrls" : [],"tags" : [],"status": "available"}]]')
    try:
        validate(instance=data, schema=result)
    except Exception as e:
        pytest.fail(e)


test_generate_json_schemas_ok()
