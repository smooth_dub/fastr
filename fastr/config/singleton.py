"""Helper to create Singleton"""


class Singleton(type):
    """Wrapper to create a singleton.

    Args:
        type ([type]): /

    Returns:
        Singleton: returns always the same instance.
    """
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(
                Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]
