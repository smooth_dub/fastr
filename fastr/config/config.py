'''Configuration singleton'''

from fastr.config.security_type import SecurityType
from fastr.config.singleton import Singleton


class Config(metaclass=Singleton):
    """Singleton that contains the configuration of the script.

    Args:
        metaclass (Singleton, optional): Defaults to Singleton.
    """

    def __init__(self) -> None:
        self.__force_mode = False
        self.__max_depth = 5
        self.__oas3_converter_options = {"supportPatternProperties": True}
        self.__security_type = SecurityType.JWT

    def get_force_mode(self) -> bool:
        return self.__force_mode

    def set_force_mode(self, force_mode: bool):
        self.__force_mode = force_mode

    def get_max_depth(self) -> int:
        return self.__max_depth

    def set_max_depth(self, max_depth: int):
        self.__max_depth = max_depth

    def get_security_type(self) -> SecurityType:
        return self.__security_type

    def set_security_type(self, security_type: SecurityType):
        self.__security_type = security_type

    def get_oas3_converter_options(self) -> dict:
        return self.__oas3_converter_options
