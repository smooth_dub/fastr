'''Allowed Security types'''
from enum import Enum


class SecurityType(Enum):
    NONE = 'none'
    JWT = 'jwt'
    API_KEY = 'key'
