"""Import all submodules"""
from fastr.config.config import Config
from fastr.config.templates import (CONFIG_HEADER_TEMPLATE, HEADER_TEMPLATE,
                                    TEST_TEMPLATE, SCHEMA_CHECK_TEMPLATE, POWERSHELL_EXEC_SCRIPT, BASH_EXEC_SCRIPT)
