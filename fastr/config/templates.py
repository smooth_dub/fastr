'''All template used to generate files.'''

CONFIG_HEADER_TEMPLATE = '''
JWT = \'\'
BASE_URL = \'\'
'''

HEADER_TEMPLATE = '''
import requests
from .config import JWT, BASE_URL
from .schemas import *
from fastr.utils import assert_status, assert_schema

{}
'''

TEST_TEMPLATE = '''
@assert_status({status_code}){assert_response}
def test_{method}_{writable_path}_ok():
    url = BASE_URL + '{path}'{optional_format}{params}{body}
    response = requests.{method}(url, header={header}{params_injection}{body_injection})
    # Add additionnal tests or logic here.
    return response
'''

SCHEMA_CHECK_TEMPLATE = '\n@assert_schema({schema})'


POWERSHELL_EXEC_SCRIPT = '''
$old_location = Get-Location
Set-Location $PSScriptRoot
python -m pytest .
Set-Location $old_location
'''

BASH_EXEC_SCRIPT = '''
old_location=$(pwd)
cd $(dirname $0)
python -m pytest .
cd $old_location
'''
