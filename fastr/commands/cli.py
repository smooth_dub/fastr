import click
from fastr.commands.generate_tests import generate


@click.group()
def cli():
    pass


cli.add_command(generate)

if __name__ == '__main__':
    cli()
