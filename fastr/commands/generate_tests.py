from fastr.models.test import Test
from fastr.config.security_type import SecurityType
import click
from cprint import *
import os

from fastr.config import Config
from fastr.utils import yaml_file_to_dict
from fastr.models.test_collection import TestCollection


@click.command()
@click.option('--dir', '-d', help='Folder in which you want to create your tests.')
@click.option('--contract', '-c', help='OAS3 contract to generate tests from.')
@click.option('--force', '-f', help='Automatically overwrite all tests files.', prompt_required=False, is_flag=True)
@click.option('--max_depth', '-m', help='Max depth allowed when resolving OAS components.', prompt_required=False)
@click.option('--security_type', '-s', help='Max depth allowed when resolving OAS components.', prompt_required=False)
def generate(dir, contract, force, max_depth, security_type):

    # Prepare global configuration
    config = Config()
    if force:
        config.set_force_mode(force)
    if max_depth:
        config.set_max_depth(int(max_depth))
    if security_type:
        config.set_security_type(SecurityType(security_type))

    # Check parameters
    if not os.path.isdir(dir):
        raise Exception('Given target is not a directory.')

    contract = yaml_file_to_dict(contract)
    # Create TestCollection
    cprint.info('Getting information from contract...')
    collections = TestCollection.generate_test_collections_from_contract(
        contract)
    cprint.ok('Information retrieved successfully.')

    cprint.info('Starting to generate files...')
    # Write output
    TestCollection.write_config_file(dir)
    TestCollection.write_scripts(dir)
    TestCollection.write_init_file(dir)
    for collection in collections:
        collection.write(dir)
    TestCollection.write_schemas_file_from_test_collection(dir, collections)

    cprint.ok('Done')


if __name__ == '__main__':
    generate()
