"""Decorator for helping user to develop unit tests"""
from functools import wraps
from jsonschema import validate, ValidationError
from requests import Response
import pytest


def assert_status(status_code: int):
    """Assert that the return code of the response match the
        given status_code.

    Args:
        status_code (int): The expected status code.
    """
    def inner_function(function):
        @wraps(function)
        def wrapper(*args, **kwargs):
            response = function(*args, **kwargs)
            if not isinstance(response, Response):
                raise TypeError(
                    "The output of the function is not a Response type.")
            assert response.status_code == status_code
            return response
        return wrapper
    return inner_function


def assert_schema(schema: dict):
    """Assert that the body of the response match the
        given json schema.

    Args:
        schema (dict): The json schema of the response.
    """
    def inner_function(function):
        @wraps(function)
        def wrapper(*args, **kwargs):
            response = function(*args, **kwargs)
            if not isinstance(response, Response):
                raise TypeError(
                    "The output of the function is not a Response type.")
            try:
                validate(instance=response.json(), schema=schema)
            except ValidationError as error:
                pytest.fail(error)
            return response
        return wrapper
    return inner_function
