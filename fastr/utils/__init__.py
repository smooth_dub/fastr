"""__init__ of the utils module"""
import typing
import re
import yaml
from cprint import *
from fastr.utils.oas3_utils import (
    generate_json_schemas, generate_writable_body, generate_optional_format)
from fastr.utils.decorators import assert_schema, assert_status


def yaml_file_to_dict(file_location) -> dict:
    """Load a yaml file into a dict

    Args:
        file_location (str): Path to the file to load

    Returns:
        dict: A dict containing the yaml content.
    """
    with open(file_location) as document:
        contract = yaml.load(document, yaml.FullLoader)
    return __resolve_references(contract, contract)


def rewrite_path(path: str) -> str:
    """Transform a path to a function name

    Args:
        path (str): The path to be transformed

    Returns:
        str: The function name matching the path
    """
    result = re.sub(r'(?<!^)(?=[A-Z])', '_', path).lower()
    return result.replace('/', '_').replace('{', 'by_').replace('}', '')[1:]


def __resolve_references(dictionnary: dict, contract: dict) -> dict:
    for key, value in dictionnary.items():
        if key == '$ref':
            reference = __find_reference(contract, value)
            return reference
        if isinstance(value, dict):
            dictionnary[key] = __resolve_references(value, contract)
    return dictionnary


def __find_reference(contract: dict, reference: dict) -> dict:
    paths = reference.split('/')
    return contract[paths[1]][paths[2]][paths[3]]


def extract_query_params(definition: dict) -> typing.List[str]:
    """Extract the query params from the OAS3 contract

    Args:
        definition (dict): The contract of the method.

    Returns:
        typing.List[str]: A list of str that contains the query params.
    """
    return __extract_params(definition, 'query')


def extract_path_params(definition: dict) -> typing.List[str]:
    """Extract the path params from the OAS3 contract

    Args:
        definition (dict): The contract of the method.

    Returns:
        typing.List[str]: A list of str that contains the paths params.
    """
    return __extract_params(definition, 'path')


def __extract_params(definition: dict, element: str) -> typing.List[str]:
    """Returns the list of element find under the parameters section
    of a OAS3 contract for a method.

    Args:
        definition (dict): The contract of the method.
        element (str): 'path' or 'query'.

    Returns:
        typing.List[str]: A list of parameters.
    """
    if element not in ['path', 'query']:
        raise Exception('element not valid.')
    results = []
    if 'parameters' in definition:
        for item in definition['parameters']:
            if item['in'] == element:
                results.append(item['name'])
    return results


def ask_overwrite_confirmation(file: str) -> bool:
    """Ask for user input if he want to overwrite an existing file.

    Args:
        file (str): file to overwrite.

    Returns:
        bool: True if the user say yes, false if not.
    """
    val = input(
        f'{file} already exists. Do you want to overwrite it ? (y/n)')
    if val not in ('y', 'yes'):
        cprint.info(f'{file} not overwritten')
        return False
    return True
