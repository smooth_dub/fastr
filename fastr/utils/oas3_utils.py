"""Utils functions to extract information from OAS3 schemas."""

import typing
from openapi_schema_to_json_schema import to_json_schema
from fastr.config import Config


def generate_json_schemas(definition: dict) -> typing.List[dict]:
    """Generate a dict of json schemas where the key is the
    associated return code.

    Args:
        definition (dict): A path definition of a OAS3 contract.

    Returns:
        typing.List[dict]: a dictionnary of schemas where the key
        is the associated return code.
    """
    schemas = dict()
    for key, value in definition['responses'].items():
        if 'application/json' in value['content']:
            schemas[key] = to_json_schema(
                value['content']['application/json'], Config().get_oas3_converter_options())['schema']
        elif '*/*' in value['content']:
            schemas[key] = to_json_schema(
                value['content']['*/*'], Config().oas3_converter_options)['schema']
    return schemas


def generate_optional_format(path_params: typing.List[str]) -> str:
    """Returns an empty str or the correct format to append
    uri parameters.
    Returns:
        str: The good format to append to the output.
    """
    if path_params:
        params = []
        for item in path_params:
            params.append(f"""{item} = ''""")
        content = ', '.join(params)
        return f".format({content})"
    return ''


def generate_writable_params(query_params: typing.List[str]) -> str:
    """Returns an empty str or the correct format to append
    query parameters.
    Returns:
        str: The good format to append to the output
    """
    if query_params:
        content = '\n    parameters = {\n'
        parameters = []
        for item in query_params:
            parameters.append(f"        '{item}' : ''")
        content += ',\n'.join(parameters)
        content += '\n    }'
        return content
    return ''


def generate_writable_body(definition: dict) -> str:
    """Returns a str of the body python representation

    Args:
        definition (dict): A path definition from a OAS3 contract.

    Returns:
        str: The python repreentation of the body
    """
    content = ''
    if 'requestBody' in definition:
        if 'application/json' in definition['requestBody']['content']:
            content = __generate_writable_body_from_content(
                definition['requestBody']['content']['application/json']['schema'], 0)
        if '*/*' in definition['requestBody']['content']:
            content = __generate_writable_body_from_content(
                definition['requestBody']['content']['*/*']['schema'], 0)
    return content


def __generate_writable_body_from_content(definition: dict, depth: int) -> str:
    depth += 1
    header = '\n    body = {\n'
    content = []
    if depth >= Config().max_depth:
        raise IndexError(
            'Too many imbricated definitions. Update the max depth parameter or update your schema')
    if 'properties' in definition:
        for key, value in definition['properties'].items():
            if definition['properties'][key]['type'] == 'object':
                content.append(
                    __generate_writable_body_from_content(value, depth))
            else:
                content.append(f"\t\t'{key}' : ''")
    if 'array' in definition:
        for key, value in definition['items'].items():
            if definition['properties'][key]['type'] == 'object':
                content.append(
                    __generate_writable_body_from_content(value, depth))
            else:
                content.append(f"\t\t{key} = ''")
    content = ',\n'.join(content)
    footer = '\n\t}'
    return header + content + footer
